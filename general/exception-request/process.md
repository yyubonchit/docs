# Exception Request

## After the 7th

Once the [stable branch is
frozen](../release-candidates.md#feature-freeze-rc), everyone needs to follow
this
[process](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/PROCESS.md#after-the-7th).

## Asking for an exception request

If you think a merge request should go into a release candidate (RC) or patch even though it does not meet these requirements,
you can ask for an exception to be made.

**Do not** set the relevant `Pick into X.Y` label (see above) before requesting
an exception; this should be done after the exception is approved.

Whether an exception is made is determined by weighing the benefit and urgency of the change
(how important it is to the company that this is released right now instead of in a month)
against the potential negative impact
(things breaking without enough time to comfortably find and fix them before the release on the 22nd).
When in doubt, we err on the side of _not_ cherry-picking.

For example, it is likely that an exception will be made for a trivial 1-5 line performance improvement
(e.g., adding a database index or adding eager loading to a query), but not for a new feature, no matter how relatively small or thoroughly tested.

All MRs which have had exceptions granted must be merged by the 15th.

### Developer

If you are a Developer asking for an exception request, check the [developer guide](developer.md)

### Release Manager

If you are a Release Manager, check the [release manager guide](release-manager.md)
